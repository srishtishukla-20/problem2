function problem2(inventory){
    for (let i = 0; i < inventory.length; i++){
        if (inventory[i] === inventory[inventory.length - 1]) {
            return inventory[i]
        }
    }
    return inventory
}
module.exports=problem2;
